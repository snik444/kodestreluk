﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace kinopoisk.Models
{
    class C_Genr
    {
        public int genreID { get; set; }
        public string genreName { get; set; }
    }

    class Data
    {
        public C_Genr[] genreData;
    }

    class C_film
    {
        public int id { get; set; }
        public string nameRU{ get; set; }
        public string country { get; set; }
        public string genre { get; set; }
        public string rating { get; set; }
        public string year { get; set; }
        public string videoURL { get; set; }
    }

    class DataFilm
    {
        public C_film[] filmsData;
    }



    class C_cinema
    {
        public string cinemaName { get; set; }
        public string address { get; set; }
        public string [] seance { get; set; }
        
    }

    class DataCinema
    {
        public C_cinema[] items;
    }

    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
     

        public ComboboxItem(string t, object o)
        {
            this.Text = t;
            this.Value = o;
        }
    }
}
