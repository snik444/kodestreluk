﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using kinopoisk.Models;
using Newtonsoft.Json;
using System.Net;



namespace kinopoisk
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Data data = JsonConvert.DeserializeObject<Data>(new WebClient().DownloadString("http://api.kinopoisk.cf/getGenres"));
            foreach (C_Genr item in data.genreData)
            {
                Genres.Add(new C_Genr() { genreID = item.genreID, genreName = item.genreName});
            }
            GenresList.ItemsSource = Genres;
            CBox.Items.Add(new ComboboxItem("Москва", 1));
            CBox.Items.Add(new ComboboxItem("Питер", 2));
            CBox.Items.Add(new ComboboxItem("Калиниград", 490));
            CBox.Items.Add(new ComboboxItem("Киев", 49));
            CBox.Items.Add(new ComboboxItem("Харьков", 732));
            CBox.Items.Add(new ComboboxItem("Донецк", 370));
            CBox.Items.Add(new ComboboxItem("Одеса", 731));
            CBox.Items.Add(new ComboboxItem("Днепропетровск", 733));




            

        }       

        List<C_Genr> Genres = new List<C_Genr>();
        List<C_film> Films = new List<C_film>();


        private void Metod_Cinema(int id)
        {
            ComboboxItem itm = (ComboboxItem)CBox.SelectedValue;
            int CityId;
            try
            {
                CityId = (int)itm.Value;
            }
            catch
            {
                CityId = 490;
            }
            List<C_cinema> C_cinemas = new List<C_cinema>();
            CinemaList.ItemsSource = null;
            try
            {
                DataCinema data = JsonConvert.DeserializeObject<DataCinema>(new WebClient().DownloadString("http://api.kinopoisk.cf/getSeance?filmID=" + id.ToString() + "&cityID=" + CityId.ToString()));
                foreach (C_cinema item in data.items)
                {
                    C_cinemas.Add(new C_cinema() { cinemaName = item.cinemaName, address = item.address, seance=item.seance });
                }
            }
            catch(NullReferenceException)
            {
                string[] s = {""};
                C_cinemas.Add(new C_cinema() { cinemaName = "Сеансы на фильм отсуствуют", address = "", seance = s });
            }

            CinemaList.ItemsSource = C_cinemas;
  
        }

        private List<C_film> FilmsLoad(string Json)
        {
            CinemaList.ItemsSource = null;
            List<C_film> Films_temp = new List<C_film>();
            DataFilm data = JsonConvert.DeserializeObject<DataFilm>(Json);
            foreach(C_film item in data.filmsData)
            {
                Films_temp.Add(new C_film() { id = item.id, country = item.country, genre = item.genre, nameRU = item.nameRU, rating = item.rating, year = item.year, videoURL=item.videoURL });
            }
            return Films_temp;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FilmsList.ItemsSource = null;
            List<C_film> Films_temp = new List<C_film>();

           foreach(C_Genr a in GenresList.SelectedItems)
           {
               foreach (C_film film in FilmsLoad(new WebClient().DownloadString("http://api.kinopoisk.cf/getTodayFilms?genreID=" + a.genreID.ToString())))
               {
                   int v=0;
                   v = Films_temp.Where(x => x.id == film.id).ToArray().Count();
                   if(v==0)
                   Films_temp.Add(film);
               }
           }

           if (Films_temp.Count > 0)
               FilmsList.ItemsSource = Films_temp.Distinct().OrderByDescending(x => x.rating); 
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            FilmsList.ItemsSource = null;
            if (FindCinema.Text.Length > 0)
            {
                string s = (new WebClient().DownloadString("http://api.kinopoisk.cf/searchFilms?keyword=" + FindCinema.Text));
                s = s.Replace("searchFilms", "filmsData");
                FilmsList.ItemsSource = FilmsLoad(s).OrderByDescending(x => x.rating); 
            }
           
        }

        private void FilmsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (C_film a in FilmsList.SelectedItems)
            {
                Metod_Cinema(a.id);
                if(a.videoURL!=null)
                ME.Source = new Uri(a.videoURL);
                break;
            }
        }

        private void CinemaList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string MBS = "", name="";
            foreach (C_cinema a in CinemaList.SelectedItems)
            {
                name = a.cinemaName;
                foreach(string s in a.seance)
                {
                    MBS += s + "\n";
                }
                break;
            }
            if (MBS != "")
                MessageBox.Show("Кинотеатр \"" + name+ "\"\nСеансы:\n"+MBS);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ME.Play();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ME.Pause();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            ME.Stop();
        }

        private void CBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (C_film a in FilmsList.SelectedItems)
            {
                Metod_Cinema(a.id);
                break;
            }
        }


    }
}
